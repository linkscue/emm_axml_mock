/*
 * =============================================================================
 *
 *       Filename:  replace_offset_hex.c
 *
 *    Description:  seek offset, write hex to file
 *
 *        Version:  1.0
 *        Created:  2014年08月08日 22时17分08秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  凌伟强 (28120), 28120@Sangfor.com
 *   Organization:  Sangfor SSLT 28120
 *
 * =============================================================================
 */
#include <stdlib.h>
#include  "stdlib.h"
#include    "stdio.h"
#include    "string.h"
#include    "unistd.h"
#include    "fcntl.h"
#include    "time.h"

void replace_offset_hex(char *, int, int);
int main(int argc, char *argv[])
{
    char *file = argv[1];                       /* 文件名称 */
    int offset = atoi(argv[2]);                 /* 偏移位置 */
    int hexid = atoi(argv[3]);                  /* 资源ID */
    replace_offset_hex(file, offset, hexid);
    return 0;
}
void replace_offset_hex(char *file, int offset, int hex){
    FILE *fp=NULL;
    if ( (fp=fopen(file, "rb+")) == NULL ){
        printf("error to open file %s\n", file);
        exit(1);
    }
    int old;
    fseek(fp, offset, SEEK_SET);
    fread(&old, sizeof(old), 1, fp);
    fseek(fp, offset, SEEK_SET);
    fwrite(&hex, sizeof(offset), 1, fp );
    printf("replace hex: offset 0x%08x, id %08x to id %08x\n", offset,old,hex);
    fclose(fp);
}
