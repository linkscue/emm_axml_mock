#!/bin/bash
# 这是一个普通程序签名脚本
# $1 -- /path/to/*.x509.pem
# $2 -- /path/to/*.pk8
# $3 -- /path/to/*.apk

dir=$(dirname $0)
jar="$dir"/signapk.jar 
pem="$1"	# key, pem，需传入Key绝对路径
pk8="$2"	# key, pk8，需传入Key绝对路径
apk="$3"	# apk，，需传入apk绝对路径
apkname=${apk%.*}
apksigned="$apkname"_Signed
zip_true=$(echo $1 | grep zip)	# 是否是*.zip命名格式，针对ROM（zip卡刷包）
if [[ $zip_true ]]; then
    output=${2:-"$apksigned.zip"}
else
    output=${2:-"$apksigned.apk"}
fi
java -jar $jar $pem $pk8 $apk $output
echo "I: signed file is $output"

