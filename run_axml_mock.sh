#!/bin/bash
#===============================================================================
#
#          FILE: run_autoinject.sh
# 
#         USAGE: ./run_autoinject.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: 凌伟强 (28120), 28120@Sangfor.com
#  ORGANIZATION: 深信服电子科技有限公司-移动应用测试部
#       CREATED: 2014年07月25日 14时40分46秒 CST
#      REVISION:  ---
#===============================================================================

# 自身信息
self=$(readlink -f $0)
self_dir=$(dirname $self)

# 工具目录
apktool=$self_dir/tools/apktool/apktool
signapk=$self_dir/tools/signtool/tool_sign_apk_debug.sh
axmlmockdir=$self_dir/tools/axml_mock
axmlprint_jar=$axmlmockdir/AXMLPrinter2.jar

# 注入内容
inject_stuffsdir=$self_dir/inject_stuffs

# 工作目录
workdir=$self_dir/workdir
empty_apk_decode_dir=$workdir/empty_mock &&\
    rm -rf $empty_apk_decode_dir 2>/dev/null
mkdir -p $workdir                               # 工作目录，用完即删

# 框架目录|回编需要
framework_apk=$self_dir/tools/apktool/framework/1.apk
test -e ~/apktool/framework/1.apk ||\
    $apktool if $framework_apk

# PATH for aapt
export PATH=$self_dir/tools/apktool:$PATH

# 编译工具
bgrep=$self_dir/bgrep
replace_offset_hex=$self_dir/replace_offset_hex
if [[ ! -e $bgrep ]]; then
    (cd $self_dir; gcc bgrep.c -o bgrep)
fi
if [[ ! -e $replace_offset_hex ]]; then
    (cd $self_dir; gcc replace_offset_hex.c -o $replace_offset_hex)
fi

# Funcion Start
#===============================================================================
# 输出信息
info(){
    echo -e "\e[0;32m==> ${@}\e[0m"
}

# 输出次级信息
infosub(){
    echo -e "\e[0;36m  --> ${@}\e[0m"
}

# 输出提示
tip(){
    echo -e "\e[0;35m==> ${@}\e[0m"
}

# 错误信息
err(){
    echo -e "\e[0;31m==> ${@}\e[0m"
}

# 次级错误信息
errsub(){
    echo -e "\e[0;31m  --> ${@}\e[0m"
}

# 查看程序包名
get_apk_package(){
   aapt d badging $1 | grep -F 'package: name=' | awk -F"'" '{print $2}'
}

# 查找启动入口
get_apk_mainActivity(){
    aapt d badging $1 | grep -F 'launchable-activity: name=' | awk -F"'" '{print $2}'
}

# 获得apk的sdk版本
# 参数$1: apk
# 返回: apk的sdk版本, 如 android-17
get_targetSdkVersion(){
    local target=$(aapt d badging $1 |sed -n 's/targetSdkVersion://gp')
    eval echo android-$target
}

# 十进制转十六进制
# 参数$1: 十进制数
# 返回: 十六进制数，如 7f020001
dec2hex(){
    printf "%x\n" $1
}

# 解析A.xml
axml_decode(){
    local axml_binary_file=$1
    local axml_decode_file=$2
    local tmpfile=/tmp/$(basename $0)_$(mktemp -u XXXXXX)_$$.apk
    local tmpdir=${tmpfile%.apk}

    # 伪造一个虚拟apk，只含一个AndroidManifest.xml
    # 然后使用apktool强制对这个虚拟apk进行解析，解决直接解析真正apk报错
    (cd $(dirname $axml_binary_file); zip $tmpfile AndroidManifest.xml)
    $apktool d -f --keep-broken-res -s $tmpfile $tmpdir 2>/dev/null
    cp -vf $tmpdir/AndroidManifest.xml $axml_decode_file
    file $axml_decode_file

    # 因为apktool解析出来的是十进制数，现把十进制转换十六进制数
    grep -o '@[0-9]\+' $axml_decode_file | sort -V | uniq | while read line; do 
        dec=${line#@}
        hex=$(dec2hex $dec)
        sed -i "s/@$dec/@$hex/g" $axml_decode_file
    done
    infosub "输出是 $axml_decode_file"
    rm -rf $tmpdir $tmpfile
}

# 获取资源ID并保存至文件
get_resid(){
    local axml_decode_file=$1
    local axml_resid_origin=$2
    grep -o '@[0-9a-z]\+' $axml_decode_file|sort|uniq|\
        awk -F'@|"' '{print $2}'|sort|tr A-Z a-z \
        >$axml_resid_origin
    sed -i 's/android://g' $axml_resid_origin
    infosub "输出是 $axml_resid_origin，前五行内容如下："
    cat $axml_resid_origin | head -n5
}

# 根据资源ID获取资源信息
get_resource(){
    local apk_origin=$1
    local apk_resid_origin=$2
    aapt d resources $apk_origin >$apk_resid_origin
    aapt d resources ~/apktool/framework/1.apk >>$apk_resid_origin
    infosub "输出是 $apk_resid_origin，前五行内容如下："
    cat $apk_resid_origin | head -n5
}

# 资源ID和资源的对照表
get_resource_table(){
    local axml_resid_origin=$1
    local axml_resid_reverse=$2
    local apk_resid_origin=$3
    local table_resid_res=$4
    >$axml_resid_reverse
    for i in $(cat $axml_resid_origin); do
        grep $i $apk_resid_origin |head -n1 |awk -F':' '{print $2}' >>$axml_resid_reverse
    done
    paste $axml_resid_origin $axml_resid_reverse | uniq >$table_resid_res
    infosub "输出是 $table_resid_res，前十行内容如下："
    cat $table_resid_res | head -n10
}

replace_id_res(){
    local axml_resid_origin=$1
    local axml_decode_file_revid=$2
    local table_resid_res=$3
    local id=""
    local res=""
    for i in $(cat $axml_resid_origin); do
        id=$i
        res=$(grep $i $table_resid_res|head -n1| awk '{print $2}')
        res=${res/\//\\\/}                      # 转换 / 为 \/ 用于正则
        infosub "replace $id $res"
        sed -i "s/$id/$res/g" $axml_decode_file_revid
    done
}

# 伪造资源
# 传入参数： 类型 string/app_name
android_res_mock(){
    dir=$1
    res=$2
    restype=$(echo $res|awk -F'/' '{print $1}')
    info "正在处理 $res"
    # TODO: 还有没有其他值未考虑到？
    case $restype in
        xml )
            path=$dir/res/${res}.xml
            mkdir -p $(dirname $path)
            cp $axmlmockdir/empty_xml.xml $path
            ;;
        drawable )
            path=$dir/res/${res}.png
            mkdir -p $(dirname $path)
            touch $path
            ;;
        string )
            path=$dir/res/values/strings.xml
            mkdir -p $(dirname $path)
            string_name=$(echo $res|awk -F'/' '{print $2}')
            string_model='    <string name="sangfor_emm_string_modol">sangfor_emm_string_modol</string>'
            string_model_file=/tmp/sangfor_emm_string_model.txt
            string_empty_file=$axmlmockdir/empty_strings.xml
            echo "$string_model" >$string_model_file
            sed -i "s/sangfor_emm_string_modol/$string_name/g" $string_model_file
            test ! -e $path && cp $string_empty_file $path
            grep -qF \"$string_name\" $path || sed -i "2 r $string_model_file" $path
            rm -f $string_model_file
            ;;
        style )
            path=$dir/res/values/styles.xml
            mkdir -p $(dirname $path)
            style_name=$(echo $res|awk -F'/' '{print $2}')
            style_model='    <style name="Sangfor_EMM_Style_Model" parent="android:Theme.Light"></style>'
            style_model_file=/tmp/sangfor_emm_style_model.txt
            style_empty_file=$axmlmockdir/empty_styles.xml
            echo "$style_model" >$style_model_file
            sed -i "s/Sangfor_EMM_Style_Model/$style_name/g" $style_model_file
            test ! -e $path && cp $style_empty_file $path
            grep -qF \"$style_name\" $path || sed -i "1 r $style_model_file" $path
            rm -f $style_model_file
            ;;
        dimen )
            path=$dir/res/values/dimens.xml
            mkdir -p $(dirname $path)
            dimen_name=$(echo $res|awk -F'/' '{print $2}')
            dimen_model='    <dimen name="Sangfor_EMM_Dimen_Model">8dp</dimen>'
            dimen_model_file=/tmp/sangfor_emm_dimen_model.txt
            dimen_empty_file=$axmlmockdir/empty_dimens.xml
            echo "$dimen_model" >$dimen_model_file
            sed -i "s/Sangfor_EMM_Dimen_Model/$dimen_name/g" $dimen_model_file
            test ! -e $path && cp $dimen_empty_file $path
            grep -qF \"$dimen_name\" $path || sed -i "1 r $dimen_model_file" $path
            rm -f $dimen_model_file
            ;;
        bool )
            path=$dir/res/values/bools.xml
            mkdir -p $(dirname $path)
            bool_name=$(echo $res|awk -F'/' '{print $2}')
            bool_model='    <bool name="Sangfor_EMM_Bool_Model">true</bool>'
            bool_model_file=/tmp/sangfor_emm_bool_model.txt
            bool_empty_file=$axmlmockdir/empty_bools.xml
            echo "$bool_model" >$bool_model_file
            sed -i "s/Sangfor_EMM_Bool_Model/$bool_name/g" $bool_model_file
            test ! -e $path && cp $bool_empty_file $path
            grep -qF \"$bool_name\" $path || sed -i "1 r $bool_model_file" $path
            rm -f $bool_model_file
            ;;
        integer )
            path=$dir/res/values/integers.xml
            mkdir -p $(dirname $path)
            integer_name=$(echo $res|awk -F'/' '{print $2}')
            integer_model='    <integer name="Sangfor_EMM_Integer_Model">8</integer>'
            integer_model_file=/tmp/sangfor_emm_integer_model.txt
            integer_empty_file=$axmlmockdir/empty_integers.xml
            echo "$integer_model" >$integer_model_file
            sed -i "s/Sangfor_EMM_Integer_Model/$integer_name/g" $integer_model_file
            test ! -e $path && cp $integer_empty_file $path
            grep -qF \"$integer_name\" $path || sed -i "1 r $integer_model_file" $path
            rm -f $integer_model_file
            ;;

        * )
            err "未知资源类型，请反馈给 lwq@sangfor.com.cn"
            exit 1
    esac
}

# 通过修改二进制，替换资源ID
hex_change_offset(){
    local file=$1
    local offset=$2
    local resid=$((0x$3))                       # 资源ID，十进制数
    $replace_offset_hex $file $offset $resid
}

# 搜索二进制
hex_search_file(){
    local hex=$1
    local file=$2
    # 计算存储数据时是小端存储（先存低位再存高位）
    # 举例：
    # 一个数字 0x7f030261
    # 计算存储 \x61 \02 \x03 \x7f
    # 每次计算机操作是一个字节，所以'看起来'是反过来的
    # ==> Google “小端存储”，网络世界是“大端存储”
    local p1="${hex:6:2}"
    local p2="${hex:4:2}"
    local p3="${hex:2:2}"
    local p4="${hex:0:2}"
    local real_hex=$p1$p2$p3$p4
    $bgrep $real_hex $file
}

# 循环替换资源ID
hex_change_all(){
    local resid_table=$1                        # 新旧资源ID对照表
    local axml_origin=$2
    local axml_mock=$3
    local offset_diff=0                         # mock_offset - origin_offset

    local tmpfile_resid_origin=/tmp/$(basename $0)_$(mktemp -u XXXXXX).resid_offsets
    local tmpfile_resid_mock=/tmp/$(basename $0)_$(mktemp -u XXXXXX).resid_offsets
    local resid_mock=0
    local resid_orgin=0
    local offset_new=0

    local match_count_mock=0
    local match_count_origin=0

    tip "资源ID对照表: "
    cat $resid_table
    tip "开始执行资源替换: "
    cat $resid_table|while read line; do
        resid_mock=$(echo $line|awk '{print $1}')
        resid_orgin=$(echo $line|awk '{print $2}')

        # 二进制搜索
        hex_search_file $resid_mock $axml_mock >$tmpfile_resid_mock
        hex_search_file $resid_orgin $axml_origin >$tmpfile_resid_origin

        # TODO: 怎么预防 - 搜索到的数量不同？
        # 为什么这么做？
        # 假定要替换 0x7f0a0000 为 0x7f0a0300
        # 在 '空壳'apk中 搜索 '\x00\x00\x0a\x7f'，数量是5个
        # 在 '原始'apk中 搜索 '\x00\x03\x0a\x7f'，数量是3个
        # 那么，直接替换 0x7f0a0000 为 0x7f0a0300 会出现 潜在的隐患
        # 优化建议：
        # 理解 aapt 存储资源ID的详细，前后做匹配，减少替换风险
        match_count_mock=$(wc -l $tmpfile_resid_mock|awk '{print $1}')
        match_count_origin=$(wc -l $tmpfile_resid_origin|awk '{print $1}')
        if [[ $match_count_mock -eq 0 ]]; then
            errsub "在'空壳'apk中，未找到资源ID: $resid_mock"
            exit 2
        fi
        if [[ $match_count_mock -eq 0 ]]; then
            errsub "在'原始'apk中，未找到资源ID: $resid_origin"
            exit 2
        fi
        if [[ $match_count_mock -ne $match_count_origin ]]; then
            errsub "搜索资源ID $resid_origin 和资源ID $resid_origin 数量上不同，继续替换有风险"
            exit 2
        else
            # 只有搜索数量一样，才执行替换
            # 取出要替换资源ID的偏移位置，一一替换
            cat $tmpfile_resid_mock|while read offset_; do
                hex_change_offset $axml_mock $offset_ $resid_orgin
            done
        fi

    done
    rm -rf $tmpfile_resid_mock $tmpfile_resid_origin
}
#===============================================================================
# Funtion END

# Error Detect, START
#===============================================================================
# android
if [[ $(which android) == "" ]]; then
    err "找不到命令 'android', 请安装Android SDK并保证已安装至少一个Target"
    exit 2
fi

# aapt
if [[ $(which aapt) == "" ]]; then
    err "找不到命令 'aapt', 请安装Android SDK并保证已配置好环境变量"
    exit 2
fi

# java
if [[ $(which java) == "" ]]; then
    err "找不到命令 'java', 请安装JDK，推荐安装JDK6"
    exit 2
fi
#===============================================================================
# Error Detect，END

cd $self_dir
# 原始apk|可有多个
apk_origins=$(find $self_dir/apk_origin -type f -name \*.apk | sort -V)
for apk_origin in $apk_origins; do
    # 
    # apk_origin: 原始apk
    # apk_decodedir: 反编译输出目录
    # apk_sangfor: 回编译输出文件
    # 
    apk_decodedir=$workdir/$(basename $apk_origin)
    apk_decodedir=${apk_decodedir%.apk}         # 清除.apk后缀
    apk_sangfor=$self_dir/apk_sangfor/$(basename $apk_origin)
    tip "正在处理: $apk_origin"
    
    # 开始反编译
    tip "反编译输出目录: $apk_decodedir"
    rm -rf $apk_decodedir 2>/dev/null
    $apktool d -r -s -f $apk_origin $apk_decodedir    # 执行反编译

    # 准备需要使用到的变量
    axml_binary_file=$apk_decodedir/AndroidManifest.xml # 原始二进制axml
    axml_decode_file=$apk_decodedir/AndroidManifest.xml2 # 带资源id的axml
    axml_decode_file_revid=$apk_decodedir/AndroidManifest.xml3 # 正常的axml
    axml_resid_origin=$apk_decodedir/axml_resid.txt # 保存axml资源id
    axml_resid_reverse=$apk_decodedir/axml_resid_reverse.txt # axml资源id对应资源

    apk_resid_origin=$apk_decodedir/app_resid.txt # 整个apk的资源信息

    table_resid_res=$apk_decodedir/table_resid_res.txt # id和资源对照表

    # 从这里开始放入注入的内容
    # 第一步：反编译期望修改的'AndroidManifest.xml文件'
    info "第一步：反编译期望修改的'AndroidManifest.xml文件'"
    axml_decode $axml_binary_file $axml_decode_file 

    # 第二步：得到AndroidManifest.xml文件的'资源ID_原始'
    info "第二步：得到AndroidManifest.xml文件的'资源ID_原始'"
    get_resid $axml_decode_file $axml_resid_origin

    # 第三步：根据资源ID获得'资源信息'（aapt d resource *.apk）
    info "第三步：根据资源ID获得'资源信息'（aapt d resource *.apk）"
    get_resource $apk_origin $apk_resid_origin

    # 第四步：根据资源信息，替换'资源ID' => '资源'（如@string/app_name）
    #         输出是正常的'AndroidManifest.xml'
    info "第四步：根据资源信息，替换'资源ID' => '资源'（如@string/app_name）"
    get_resource_table $axml_resid_origin $axml_resid_reverse $apk_resid_origin $table_resid_res

    # 替换 '资源ID 7f0202001' ==> '资源 @string/xxx..'
    cp $axml_decode_file $axml_decode_file_revid
    replace_id_res $axml_resid_origin $axml_decode_file_revid $table_resid_res

    # 第五步：生成空壳apk工程
    info '第五步：生成空壳apk工程'
    project=$workdir/app
    package=$(get_apk_package $apk_origin)          # 程序包名
    launcher=$(get_apk_mainActivity $apk_origin)    # 桌面启动入口
    launcher_=${launcher//\./\/}                    # 转换 '.' 为 '/'
    launcher_activity=$(basename $launcher_)        # 只要类名称
    rm -rf $project
    mkdir -p $project

    # 优先考虑原apk所需求的target
    use_targetSdkVersion=0
    apk_origin_target_version=$(get_targetSdkVersion $apk_origin)
    # 首先去查找当前安装的Android SDK是否有对应的target(例如: android-17)
    if (android list target | grep -q $apk_origin_target_version); then
        use_targetSdkVersion=$apk_origin_target_version
    else
        # android_target_last 用户已安装的SDK中最新Target, 如android-19
        android_target_last=$(android list targets| grep 'android-' | tail -n1 | awk -F'"' '{print $2}')
        use_targetSdkVersion=$android_target_last
    fi
    android create project -n sangfor -t $use_targetSdkVersion -p $(readlink -f $project) -k $package -a $launcher_activity
    if [[ $? -ne 0 ]]; then
        err "创建Android工程失败，请检查错误信息"
        exit 2
    fi

    # TODO: 在这里执行注入权限的操作，操作文件：$axml_decode_file_revid

    # 第六步：替换'AndroidManifest.xml'，伪造资源，并回编译'空壳apk'
    info "第六步：替换'AndroidManifest.xml'，伪造资源，并回编译'空壳apk'"
    # 因为资源ID的替换比较复杂，单独与AndroidManifest.xml分开写函数
    cp -vf $axml_decode_file_revid $project/AndroidManifest.xml
    for i in $(cat $axml_resid_reverse); do
        android_res_mock $project $i
    done
    (cd $project && ant debug)

    # 第七步：再反编译重新生成的 '空壳apk'，取得'资源ID_空壳'
    info "第七步：再反编译重新生成的 '空壳apk'，取得'资源ID_空壳'"
    apk_debug=$(find $project/bin -type f -name \*debug.apk)
    apk_debug_decodedir=$workdir/app_debug 
    rm -rf $apk_debug_decodedir 2>/dev/null
    $apktool d -r -s -f $apk_debug $apk_debug_decodedir
    apk_debug_axml_binary=$apk_debug_decodedir/AndroidManifest.xml
    apk_debug_axml_decode=$apk_debug_decodedir/AndroidManifest.xml2
    apk_debug_axml_decode_resid=$apk_debug_decodedir/AndroidManifest.xml3
    apk_debub_axml_resid=$apk_debug_decodedir/resid.txt
    apk_debug_axml_resid_reverse=$apk_debug_decodedir/resid_reverse.txt
    apk_debug_table_resid_res=$apk_debug_decodedir/table_resid_res.txt
    apk_debug_resid_origin=$apk_debug_decodedir/app_resid.txt # 整个apk的资源信息
    axml_decode $apk_debug_axml_binary $apk_debug_axml_decode
    get_resid $apk_debug_axml_decode $apk_debub_axml_resid
    get_resource $apk_debug $apk_debug_resid_origin
    get_resource_table $apk_debub_axml_resid $apk_debug_axml_resid_reverse $apk_debug_resid_origin $apk_debug_table_resid_res

    # 第八步：二进制替换方法，AndroidManifest.xml中的'资源ID_空壳' => '资源ID_原始'
    # TODO: 如何避免两张表资源列表不一样？
    info "第八步：二进制替换方法，AndroidManifest.xml中的'资源ID_空壳' => '资源ID_原始'"
    id_left=/tmp/$(basename $0)_$(mktemp -u XXXXXX)_$$.txt
    id_right=/tmp/$(basename $0)_$(mktemp -u XXXXXX)_$$.txt
    id_table=/tmp/$(basename $0)_$(mktemp -u XXXXXX)_$$.txt
    cat $apk_debug_table_resid_res | sort -k 2 -V | awk '{print $1}' >$id_left
    cat $table_resid_res | sort -k 2 -V | awk '{print $1}' >$id_right
    paste $id_left $id_right >$id_table
    hex_change_all $id_table $axml_binary_file $apk_debug_axml_binary 
    cp -vf $apk_debug_axml_binary $axml_binary_file

    # 开始回编译
    tip "回编译输出文件: $apk_sangfor"
    $apktool b $apk_decodedir $apk_sangfor

    # 签名操作
    $signapk $apk_sangfor
    rm $apk_sangfor                             # 删除未签名的，保留已签名的
    tip "注入${apk_origin}操作完成!"
done

# 删除工作目录
#rm -rf $workdir
