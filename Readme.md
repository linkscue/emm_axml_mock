## 自动注入

自动化注入Android权限

## 目录结构

*   apk_origin: 原始apk存放目录
*   apk_sangfor: 注入测试代码之后的apk存放目录
*   tools: 签名、反编译工具存放目录
*   run_autoinject.sh: 批量处理脚本

## 工作内容

项目团队已找到更好的解决方案：https://code.google.com/p/axml/

当前这个注入工具还有几项TODO需要完成：

*   [如何避免替换到无关字符串](http://200.200.0.36/28120/emm_axml_mock/blob/axml_mock/run_axml_mock.sh#L275)
*   [有没有未考虑到的资源没有伪造](http://200.200.0.36/28120/emm_axml_mock/blob/axml_mock/run_axml_mock.sh#L175)
*   [根据Android SDK不同修改这个值](http://200.200.0.36/28120/emm_axml_mock/blob/axml_mock/run_axml_mock.sh#L342)
*   [新旧资源ID有重复怎么处理](jhttp://200.200.0.36/28120/emm_axml_mock/blob/axml_mock/run_axml_mock.sh#L376)

## 效果展示
![android](http://200.200.0.36/28120/emm_axml_mock/raw/axml_mock/images/axml_mock.png)
